<?
# All requests to your Infinity website or app will be sent to this file.
# There are some configuration options you'll want to look over below.


# you'll want to change this in production:
ini_set('display_errors', 'On');
error_reporting(E_ALL);

# set your timezone. all dates/times will be relative to this.
# the list is located at [http://php.net/manual/en/timezones.php]
date_default_timezone_set('America/Chicago');

# start by including Infinity's bootstrap file.
require_once 'vendor/mjuhl/infinity/lib/bootstrap.php';

# create an instance of Infinity
$infinity = new \Infinity\Infinity;

# Infinity needs to know the path to your site's root directory
$local_path = dirname(__FILE__);

# set your configuration options here:
$infinity->setConfig([
    'site_name' => 'My Infinitely Awesome Website', // change this to your site's name
    'developer_email' => 'you@example.com', // change this to your email

    # Infinity needs to know the path to your site's root directory
    'root_directory' => $local_path,

    // TODO: refactor things because root_directory and website_directory are the same thing now
    # This tells Infinity where the code for your site lives. You don't need to change this.
    'website_directory' => $local_path,

    # Tell Infinity where to store data files, including your content database,
    # site encryption keys, and so forth. Optimally, this directory would be
    # located outside of your web server root somwhere not publicly accessible.
    # You definitely should change this, but Infinity will work if you don't.
    'data_directory' => $local_path . DIRECTORY_SEPARATOR . 'data',

    # Infinity creates a sqlite database file for you in the data directory specified
    # above. You can change the filename here if you're so inclined.
    'db_filename' => 'infinity_data.db',


    # If enabled, Infinity will automatically load CMS content documents from the
    # database based on the page's route. These documents live under the special
    # "site_pages" category.
    // TODO: should this category name be configurable
    'auto_load_content' => TRUE,

    # If you want to turn on Infinity's internal logging for debug purposes, set
    # a file name here to write to. Debug messages will get written to this file
    # during each request. If you don't need this functionality, comment or delete
    # this line:
    'infinity_log_file' => $local_path . DIRECTORY_SEPARATOR . 'data/infinity.log'
]);



// $infinity->view->setLayout('main-layout');


# these are convenience functions for adding routes
# you can delete these if you're not using them
function get ($url_pattern, $controller_function)
{
    global $infinity;
    $infinity->addRoute('get', $url_pattern, $controller_function);
}

function post ($url_pattern, $controller_function)
{
    global $infinity;
    $infinity->addRoute('post', $url_pattern, $controller_function);
}

# here is how you define controller logic for your routes.
# this can and probably should be done in separate file(s).
$infinity->defineRoutes([

    ['get', '/foo/bar', function ()
    {
        die('foo bar!');
    }],

    ['*', '/', function ()
    {
        die('hello world!');
    }]
]);

get('/test/this', function ()
{
    die('this works');
});

get('/about', function ($infinity, $params)
{
    $infinity->log('about!!');

});


# tell Infinity to begin processing the request
$infinity->launch();

