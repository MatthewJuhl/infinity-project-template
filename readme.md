# Infinity Site Project Starter

This project is a template for Infinity projects.

1. Download this project as an archive, and extract its contents into a new directory where you will begin work on your project.
2. Initialize dependencies using Composer.
3. Decide where your data directory will live, create it, and give set its permissions such that apache can write to it.
4. Set options in config.php.
5. Point your webserver at your root directory, launch it in a web browser and Infinity should finish initializing your site.
